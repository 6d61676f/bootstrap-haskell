module Coin where

import           Control.Arrow
import           Data.Ratio
data Coin = Head | Tail deriving (Show,Eq)
newtype PN x = PN { getPN :: [(x,Rational)] } deriving (Show)


instance Functor PN where
    --fmap f (PN xs) = PN $ map (\(x,p) -> (f x,p)) xs
    fmap f (PN xs)   = PN $ map (first f) xs
    --Nu stiu ce naiba inseamna Control.Arrow.first, dar e recomandat de ghc-mod

instance Applicative PN where
    pure x = PN [(x,1%1)]
    (<*>)  = primaChestie

instance Monad PN where
    return  = pure
    (>>=)   = aDouaChestie
    fail _  = PN []

primaChestie :: PN (a->b) -> PN a -> PN b
primaChestie (PN [(f,pf)]) (PN [(x,px)]) = PN [(f x,px)]

aDouaChestie :: PN a -> (a -> PN b) -> PN b
aDouaChestie (PN [(x,px)]) f = f x
