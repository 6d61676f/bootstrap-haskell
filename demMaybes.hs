module DemMaybes where

import Control.Monad.Writer
import System.Random

listOfMaybes = [Nothing, Just 1, Nothing, Just 2, Just 3, Nothing]

func :: [Maybe a] -> [a]
func [] = []
func (Nothing:xs) = func xs
func (Just x:xs) = x : func xs

func' :: [Maybe a] -> [a]
func' = foldr match []
  where
    match (Just a) x = a : x
    match Nothing x = x

gcd' :: Int -> Int -> Writer [String] Int
gcd' a b
  | b == 0 = do
    tell ["Finished with " ++ show a]
    return a
  | otherwise = do
    c <- pure $ mod a b
    tell [show a ++ " mod " ++ show b ++ " = " ++ show c]
    gcd' b c
{- sau
   | otherwise =
    let c = mod a b in
        tell [show a ++ " mod " ++ show b ++ " = " ++ show c]
        gcd' b c
-}

ceva :: String -> IO ()
ceva x
  | x == "pup" = putStrLn "Pup"
  | x == "moloz" = putStrLn "Moloz"
  | otherwise = return ()

randFloat :: Int -> (Float, StdGen)
randFloat = random . mkStdGen
