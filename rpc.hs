module Calculator where

rpc :: (Fractional a, Read a) => [Char] -> a
rpc expression = head . foldl evaluateExpr [] $ words expression
  where
    evaluateExpr (x : y : xs) "+" =
        (x + y) : xs
    evaluateExpr (x : y : xs) "*" =
        (x * y) : xs
    evaluateExpr (x : y : xs) "-" =
        (x - y) : xs
    evaluateExpr (x : y : xs) "/" =
        (x / y) : xs
    evaluateExpr xs strNo = read strNo : xs
