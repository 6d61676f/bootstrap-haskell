import System.IO

main = do
  withFile
    "/etc/fstab"
    ReadMode
    (\handle -> do
       contents <- hGetContents handle
       putStr contents
       return 0)
