import Data.List
import Control.Exception
import System.IO
import System.Directory
import System.Environment

dispatch :: String -> [String] -> IO ()
dispatch "add" = add
dispatch "view" = view
dispatch "remove" = remove
dispatch _ = usage

main = do
  argv <- getArgs
  if (length argv < 2)
    then usage []
    else let command = head argv
             args = tail argv
         in dispatch command args

add :: [String] -> IO ()
add [fisier, linie] = do
  appendFile fisier $ linie ++ "\n"
add _ = usage []

view :: [String] -> IO ()
view [fisier] = do
  contents <- readFile fisier
  let linii = lines contents
      frumi = zipWith (\n l -> show n ++ " - " ++ l) [0 ..] linii
  mapM_ putStrLn frumi
view _ = usage []

remove :: [String] -> IO ()
remove [fisier, number] = do
  contents <- readFile fisier
  let no = read number
      linii = lines contents
      newFile = unlines $ delete (linii !! no) linii
  bracketOnError
    (openTempFile "." "temp")
    (\(tempName, tempHandle) -> do
       hClose tempHandle
       removeFile tempName)
    (\(tempName, tempHandle) -> do
       hPutStr tempHandle newFile
       hClose tempHandle
       removeFile fisier
       renameFile tempName fisier)
remove _ = usage []

usage :: [String] -> IO ()
usage _ = do
  exec <- getProgName
  putStrLn $ exec ++ " add/view/remove 'todo.txt' ..."
