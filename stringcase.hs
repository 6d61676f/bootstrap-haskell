import qualified Data.Char as Ch

main = do
  contents <- getContents
  putStr $ map func contents
  where
    func x
      | Ch.isLower x == True = Ch.toUpper x
      | Ch.isUpper x == True = Ch.toLower x
      | otherwise = x
