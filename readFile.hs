import System.IO

main = do
  fHandle <- openFile "/etc/fstab" ReadMode
  contents <- hGetContents fHandle
  _ <- putStr contents
  hClose fHandle
