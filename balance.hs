module Balance where

type Birds = Int

type Pole = (Birds, Birds)

landLeft :: Birds -> Pole -> Either Pole Pole
landLeft x (l, r)
  | abs (r - (l + x)) > 3 = Left (l + x, r)
  | otherwise             = Right (l + x, r)

landRight :: Birds -> Pole -> Either Pole Pole
landRight = check
  where
    check x (l, r) =
      if abs (l - (r + x)) > 3
        then Left (l, r + x)
        else Right (l, r + x)

banana :: Pole -> Either Pole Pole
banana = Left
