import Data.List
import System.IO
import Control.Exception
import System.Directory

main = do
  chores <- readFile "./chores"
  let linii = lines chores
      numericLines = zipWith (\n l -> show n ++ " - " ++ l) [0 ..] linii
  mapM_ putStrLn numericLines
  putStrLn "Let's delete one.."
  numberString <- getLine
  let number = read numberString
      newList = unlines $ delete (linii !! number) linii
  bracketOnError
    (openTempFile "." "temp")
    (\(tempName, tempHandle) -> do
       hClose tempHandle
       removeFile tempName)
    (\(tempName, tempHandle) -> do
       hPutStr tempHandle newList
       hClose tempHandle
       removeFile "chores"
       renameFile tempName "chores")
