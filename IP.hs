module IP
  ( IP(..)
  , splitDatIp
  , numericIp
  , numericIp'
  , ipv4Numeric
  , binary
  , datMask
  ) where

data IP = IPv4
  { address :: [[Char]]
  , cidr :: Int
  }

instance Show IP where
  show (IPv4 a b) = "IP:" ++ show a ++ ",cidr:" ++ show b

splitDatIp :: [Char] -> [[Char]]
splitDatIp (x:xs) = foldr func [[]] (x : xs)
  where
    func x (ax:axs) =
      if x /= '.' && x /= '/'
        then (x : ax) : axs
        else [] : ax : axs

numericIp :: [[Char]] -> [Int]
numericIp x = map read x

numericIp' :: [Char] -> [Int]
numericIp' x = numericIp $ splitDatIp x

ipv4Numeric :: [Int] -> Int
ipv4Numeric x
  | length x >= 4 = foldl1 (+) $ f x
  | otherwise = 0
  where
    f x = [x !! 0 * 2 ^ 24, x !! 1 * 2 ^ 16, x !! 2 * 2 ^ 8, x !! 3]

ipv4Numeric' :: [Char] -> Int
ipv4Numeric' x = ipv4Numeric $ numericIp' x

datMask :: Int -> [Int]
datMask x
  | x >= 0 && x <= 32 =
    foldr (\bit acc -> bit : acc) (take (32 - x) $ repeat 0) (take x $ repeat 1)
  | otherwise = take 32 $ repeat 1

--mask2Number :: [Int] -> Int -> Int
--mask2Number [] _ = 0
--mask2Number (x:xs) 0 = x
--mask2Number (x:xs) bits
--  | bits >= 0 && bits <= 31 = x * 2 ^ bits + mask2Number xs (bits - 1)
--  | otherwise = 0
mask2Number :: [Int] -> Int
mask2Number (x:xs) =
  let func [] _ = 0
      func (x:xs) 0 = x
      func (x:xs) bits = x * 2 ^ bits + func xs (bits - 1)
  in func (x : xs) 31

binary
  :: (Integral a)
  => a -> [a]
binary x = reverse $ func x
  where
    func 0 = [0]
    func 1 = [1]
    func no = (no `mod` 2) : (func $ no `div` 2)
