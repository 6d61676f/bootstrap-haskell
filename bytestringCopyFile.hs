import Control.Exception
import qualified Data.ByteString.Lazy as B
import System.Directory
import System.Environment
import System.IO

main = do
  (file1:file2:_) <- getArgs
  copy file1 file2

copy src dst = do
  content <- B.readFile src
  bracketOnError
    (openTempFile "." "temp")
    (\(tName, tHandle) -> do
       hClose tHandle
       removeFile tName)
    (\(tName, tHandle) -> do
       B.hPutStr tHandle content
       hClose tHandle
       renameFile tName dst)
