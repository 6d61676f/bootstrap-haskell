module Typeclasses where

data TrafficLight = Red | Yellow | Green

instance Eq TrafficLight where
    Red == Red = True
    Green == Green = True
    Yellow == Yellow = True
    _ == _ = False

instance Show TrafficLight where
    show Red = "Red Light"
    show Yellow = "Yellow Light"
    show Green = "Green light"

class YesNo a where
    yesno :: a -> Bool

instance YesNo Int where
    yesno 0 = False
    yesno _ = True

instance YesNo (Maybe a) where
    yesno (Just a) = True
    yesno Nothing = False

instance YesNo (TrafficLight) where
    yesno Green = True
    yesno _ = False

instance YesNo [a] where
    yesno [] = False
    yesno _ = True

yesnoIf :: (YesNo y) => y -> a -> a -> a
yesnoIf val trueRet falseRet =
    if yesno val then trueRet else falseRet
