module Ceva where

import qualified Data.List as DL
import qualified Data.Char as DC

factorial
  :: (Integral a)
  => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

addTriples
  :: (Num a)
  => (a, a, a) -> (a, a, a) -> (a, a, a)
addTriples (x1, y1, z1) (x2, y2, z2) = (x1 + x2, y1 + y2, z1 + z2)

fstTrp
  :: (Num a)
  => (a, a, a) -> a
fstTrp (x, _, _) = x

sndTrp
  :: (Num a)
  => (a, a, a) -> a
sndTrp (_, x, _) = x

trdTrp
  :: (Num a)
  => (a, a, a) -> a
trdTrp (_, _, x) = x

--Fiboncci Function
fibonacci
  :: (Num a, Num t, Eq a)
  => a -> t
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci 2 = 1
fibonacci x = fibonacci (x - 1) + fibonacci (x - 2)

--Pattern Matching
capital :: [Char] -> [Char]
capital [] = "Linie libera"
capital argumente@(x:xs) =
  "Primul simbol din linia '" ++ argumente ++ "' este: " ++ [x]

--BMI Calculator
bmiCalc
  :: (Floating a)
  => a -> a -> a
bmiCalc mass height = (mass / (height ^ 2))

--BMI Interpreter
bmiTell
  :: (Ord a, Floating a)
  => a -> [Char]
bmiTell bmiIndex
  | bmiIndex <= 18.5 = "Underweight"
  | bmiIndex <= 25.0 = "Normalweight"
  | bmiIndex <= 30.0 = "Overweight"
  | otherwise = "Highly Overweight"

--BMI Calculator and Interpreter
bmiTell'
  :: (Ord a, Floating a)
  => a -> a -> [Char]
bmiTell' mass height
  | bmi <= 18.5 = "UnderWeight"
  | bmi <= 25.0 = "NormalWeight"
  | bmi <= 30.0 = "OverWeight"
  | otherwise = "Next Level"
  where
    bmi = mass / height ^ 2

--BMI Calculator and Interpreter 2.0
bmiTell''
  :: (Ord a, Floating a)
  => a -> a -> [Char]
bmiTell'' mass height
  | bmi <= skinny = "UnderWeight"
  | bmi <= normal = "NormalWeight"
  | bmi <= fat = "OverWeight"
  | otherwise = "Next Level"
  where
    bmi = mass / height ^ 2
    (skinny, normal, fat) = (18.5, 25.0, 30.0)

--BMIs Calculator
bmisCalc
  :: (Floating a)
  => [(a, a)] -> [a]
bmisCalc xs =
  [ bmi weight height
  | (weight, height) <- xs ]
  where
    bmi w h = (w / (h ^ 2))

--Circle Area
circleArea
  :: (Floating a)
  => a -> a
circleArea radius =
  let r2 = radius ^ 2
  in pi * r2

--Sphere Volume
sphereVolume
  :: (Floating a)
  => a -> a
sphereVolume radius =
  let r = (4 / 3) * radius ^ 3
  in pi * r

--Case
isZero
  :: (Num a, Ord a)
  => a -> String
isZero numar =
  "Numarul este " ++
  case numar of
    0 -> "zero."
    _ -> "nenul."

--Maximum Recursive
maxim
  :: (Ord a)
  => [a] -> a
maxim [] = error "lista goala"
maxim [x] = x
maxim (x:xs)
  | x > maxTail = x
  | otherwise = maxTail
  where
    maxTail = maxim xs

--Maximum Recursive 2
maxim'
  :: (Ord a)
  => [a] -> a
maxim' [] = error "bro,stahp"
maxim' [x] = x
maxim' (x:xs) = max x (maxim' xs)

--Replicate Recursive
replicate'
  :: (Ord a, Num a)
  => a -> b -> [b]
replicate' n x
  | n <= 0 = []
  | otherwise = x : replicate' (n - 1) x

--Take Recursive
take'
  :: (Ord a, Num a)
  => a -> [b] -> [b]
take' n (x:xs)
  | n <= 0 = []
  | otherwise = x : take' (n - 1) (xs)

--Reverse Recursive
reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

--Append Brah Noice
append' :: a -> [a] -> [a]
append' y [] = y : []
append' y (x:xs) = x : append' y xs

--Concat Brah Pwp
concat' :: [a] -> [a] -> [a]
concat' [] ys = ys
concat' (x:xs) ys = x : concat' xs ys

--Map Pwp
map' :: (t1 -> t) -> [t1] -> [t]
map' func [] = []
map' func (x:xs) = func (x) : map' func xs

--Quicksort Fuk
quickSort
  :: (Ord a)
  => [a] -> [a]
quickSort [] = []
quickSort (x:xs) =
  let lower =
        quickSort
          [ y
          | y <- xs 
          , y <= x ]
      upper =
        quickSort
          [ y
          | y <- xs 
          , y > x ]
  in lower ++ [x] ++ upper

--Euclid GCD
gcd'
  :: (Integral a, Ord a)
  => a -> a -> a
gcd' x y
  | y == 0 = x
  | otherwise = gcd' y (mod x y)

--Section Function
divideByHundred
  :: (Floating a)
  => a -> a
divideByHundred = (/ 100)

--IsAlpha
isAlpha :: Char -> Bool
isAlpha = (`elem` ['A' .. 'z'])

--Apply Function to Argument
do2it :: (a -> a) -> a -> a
do2it f x = f (f x)

--Zip With
--Sneaky As Fuk zipWith' (zipWith' (*)) [[1,2,3],[3,5,6],[2,3,4]] [[3,2,2],[3,4,5],[5,4,3]]
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith f xs ys

--isPrime
isPrime :: Integer -> Bool
isPrime a =
  null
    [ x
    | x <- [2 .. floor (sqrt (fromIntegral a))] 
    , mod a x == 0 ]

--Collatz
collatz
  :: (Integral a)
  => a -> [a]
collatz 1 = [1]
collatz x
  | even x = x : collatz (div x 2)
  | odd x = x : collatz (3 * x + 1)

--From 1 to 100 all the collatz seqs that have length ge 15
collatzEvil
  :: (Integral a)
  => [a] -> Int -> [a]
collatzEvil x l =
  [ a
  | a <- x 
  , length (collatz (a)) >= l ]

--Pythagorean numbers
pyta
  :: (Integral a)
  => a -> [(a, a, a)]
pyta n =
  [ (x, y, z)
  | x <- [1 .. n] 
  , y <- [1 .. n] 
  , z <- [1 .. n] 
  , x ^ 2 + y ^ 2 == z ^ 2 ]

--Fold Magic
maximum'
  :: (Ord a)
  => [a] -> a
maximum' =
  foldl1
    (\acc x ->
        if x > acc
          then x
          else acc)

reverse'' :: [a] -> [a]
reverse'' = foldl (\acc x -> x : acc) []

filter' :: (a -> Bool) -> [a] -> [a]
filter' f =
  foldr
    (\x acc ->
        if f x
          then x : acc
          else acc)
    []

--Tails from Data.List
tails' :: [a] -> [[a]]
tails' [] = []
tails' x = x : (tails' . tail $ x)

--Sau 
tails'' :: [a] -> [[a]]
tails'' [] = []
tails'' (x:xs) = (x : xs) : (tails'' xs)

--isPrefixOf'
isPrefixOf'
  :: (Eq a)
  => [a] -> [a] -> Bool
isPrefixOf' [] _ = True
isPrefixOf' _ [] = False
isPrefixOf' (x:xs) (y:ys)
  | x == y = isPrefixOf' xs ys
  | otherwise = False

--Needle in Haystack or IsIn
--DL.isInfixOf
isIn
  :: (Eq a)
  => [a] -> [a] -> Bool
isIn needle haystack = any (DL.isPrefixOf needle) (DL.tails haystack)

--Any
any' :: (a -> Bool) -> [a] -> Bool
any' f [] = False
any' f (x:xs)
  | f x == True = True
  | otherwise = any' f xs

--Caesar Cypher
caesar :: Int -> [Char] -> [Char]
caesar offset (x:xs) = map (\c -> DC.chr (DC.ord c + offset)) (x : xs)

--Caesar Decode
caesarD :: Int -> [Char] -> [Char]
caesarD offset (x:xs) = caesar (negate offset) (x : xs)

--sau
caesarD' :: Int -> [Char] -> [Char]
caesarD' offset (x:xs) = map (\c -> DC.chr (DC.ord c - offset)) (x : xs)

--Sum Digits
sumDig :: Int -> Int
sumDig = sum . map DC.digitToInt . show

--First Natural No that has the digit sum equal to
--'Maybe' page 117
firstTo :: Int -> Maybe Int
firstTo n = DL.find (\x -> sumDig x == n) [1 ..]
