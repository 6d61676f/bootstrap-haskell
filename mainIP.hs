import IP

main = do
  putStrLn "Give me that IP x.x.x.x/x"
  ipStr <- getLine
  let addressL = take 4 $ splitDatIp ipStr
      prefixL = read $ splitDatIp ipStr !! 4
      ipv4 =
        IPv4
        { address = addressL
        , cidr = prefixL
        }
  print ipv4
  print ((ipv4Numeric . numericIp)  (address ipv4))
  print (datMask (cidr ipv4))
